"use strict";

import readline from 'readline';

import xmpp from 'simple-xmpp';

class XmppBridge {
	/**
	 * Creates a new XmppBridge instance.
	 * @param	{string}	in_login_jid		The JID to login with.
	 * @param	{string}	in_destination_jid	The JID to send stdin to.
	 * @param	{Boolean}	in_is_groupchat		Whether the destination JID is a group chat or not.
	 */
	constructor(in_destination_jid, in_is_groupchat) {
		this.destination_jid = in_destination_jid;
		this.is_destination_groupchat = in_is_groupchat;
		
		this.client = xmpp;
		this.client.on("online", this.on_connect.bind(this));
		this.client.on("error", this.on_error.bind(this));
		this.client.on("chat", ((_from, _message) => {
			// noop
		}).bind(this));
	}
	
	start(jid, password) {
		this.client.connect({
			jid,
			password
		});
	}
	
	send(message) {
		this.client.send(
			this.destination_jid,
			message,
			this.is_destination_groupchat
		);
	}
	
	// ------------------------------------------------------------------------
	
	on_connect(data) {
		console.log(`[XmppBridge] Connected as ${data.jid}.`);
		if(this.is_destination_groupchat) {
			this.client.join(`${this.destination_jid}/bot_${data.jid.user}`);
		}
		this.stdin = readline.createInterface({
			input: process.stdin,
			output: process.stdout,
			terminal: false
		});
		this.stdin.on("line", this.on_line_handler.bind(this));
		this.stdin.on("close", this.on_stdin_close_handler.bind(this));
	}
	
	on_error(error) {
		console.error(`[XmppBridge] Error: ${error}`);
	}
	
	// ------------------------------------------------------------------------
	
	on_line_handler(line_text) {
		this.send(line_text);
	}
	on_stdin_close_handler() {
		this.client.disconnect();
	}
}

export default XmppBridge;
