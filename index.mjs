#!/usr/bin/env node
"use strict";

import XmppBridge from './XmppBridge.mjs';

const settings = {
	jid: process.env.XMPP_JID,
	destination_jid: null,
	is_destination_groupchat: false,
	password: process.env.XMPP_PASSWORD
};

let extras = [];
// The first arg is the script name itself
for(let i = 1; i < process.argv.length; i++) {
	if(!process.argv[i].startsWith("-")) {
		extras.push(process.argv[i]);
		continue;
	}
	
	switch(process.argv[i]) {
		case "-h":
		case "--help":
			console.log(`XmppBridge
    By Starbeamrainbowlabs

Usage:
	{environment variables} ${process.argv[0]} {options}

Options:
    -h  --help           Show this message
        --destination    Required. Specifies the destination JID to send stdin to
        --groupchat      Optional. Specifies that the destination JID is actually a group chat

Environment Variables:
    XMPP_JID             Required. The JID to login with.
    XMPP_PASSWORD        Required. The password to login with.
`);
			process.exit(0);
			break;
			
		case "--destination":
			settings.destination_jid = process.argv[++i];
			break;
		case "--groupchat":
			settings.is_destination_groupchat = true;
			break;
		
		default:
			console.error(`Error: Unknown argument '${process.argv[i]}'.`);
			process.exit(2);
			break;
	}
}

// ------------------------------------------------------------------------

for(let environment_varable of ["XMPP_JID", "XMPP_PASSWORD"]) {
	if(typeof process.env[environment_varable] == "undefined") {
		console.error(`Error: The environment variable ${environment_varable} wasn't found.`);
		process.exit(1);
	}
}

if(typeof settings.destination_jid != "string") {
	console.error("Error: No destination jid specified.");
	process.exit(5);
}

// ------------------------------------------------------------------------

console.log(`[XmppBridge] Sending to ${settings.destination_jid} (groupchat: ${settings.is_destination_groupchat})`);

const bridge = new XmppBridge(
	settings.destination_jid,
	settings.is_destination_groupchat
);
bridge.start(settings.jid, settings.password);
