# xmppbridge

> Simple Node.JS script that sends the standard input to a destination over XMPP - optionally to a group chat.

## Getting Started
Simply install this package like so:

```bash
sudo npm install -g xmppbridge
```

Then, call it like this:

```bash
some_command | XMPP_JID="bot@bobsrockets.com" XMPP_PASSWORD="cheese" xmppbridge --destination "bill@billsboosters.co.uk"
```

In short, ensure the `XMPP_JID` and `XMPP_PASSWORD` environment variables are present to let `xmppbridge` know how to login to the XMPP sever.

Then, specify the destination to send the messages to with the `--destination` argument - optionally adding the `--groupchat` argument if appropriate.

## Options

Environment Variable	| Meaning
------------------------|---------------------------
`XMPP_JID`				| The JID to login with
`XMPP_PASSWORD`			| The password to use when logging in

Arguments			| Meaning
--------------------|-------------------------------
`-h`, `--help`		| Show this message
`--destination`		| Required. Specifies the destination JID to send stdin to
`--groupchat`		| Optional. Specifies that the destination JID is actually a group chat

## Contributing
Contributions are welcome! Feel free to open a merge request (or send a patch via email if you prefer - my email address is on [my website](https://starbeamrainbowlabs.com/)) :-)

## Support
If you run into issues, please get in contact and I'll try to help out :-)

The best way of doing so is probably by [opening an issue](https://gitlab.com/sbrl/xmppbridge/issues/new), but an email is fine too (my email address is on [my website](https://starbeamrainbowlabs.com/)).

## License
This project is licensed under the _Mozilla Public License 2.0_ - the full text of which is located in the `LICENSE` file in this repository.
